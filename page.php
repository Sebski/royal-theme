<?php

get_header();
$news = ProductsService::getNews(1,3);
?>

<!--    <link rel="stylesheet" href="--><?php //bloginfo('template_directory'); ?><!--/assets/css/single.css"/>-->


<?php get_header();?>
<?php while ( have_posts() ) : the_post(); ?>
    <!-- Breadcrumb -->
    <div class="alice-bg padding-top-150 padding-bottom-70">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="breadcrumb-area">
                        <h1><?php the_title() ?></h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Strona główna</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><?php the_title() ?></li>
                            </ol>
                        </nav>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="breadcrumb-form">
                        <form action="#">
                            <input type="text" placeholder="Enter Keywords">
                            <button><i data-feather="search"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->

    <!-- Candidates Details -->
    <div class="alice-bg section-padding-bottom">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="white-bg others-block">
                        <div class="about-company">
                            <div class="row">
                                <div class="col-lg-6 order-lg-2">
                                    <div class="feature-thumb">
                                        <img src="images/feature/thumb-1.png" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <div class="col-lg-6 order-lg-1">
                                    <div class="feature-content">
                                       <?php the_content(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="infobox-wrap">
                            <div class="row">
                                <div class="col-lg-4 col-md-6">
                                    <div class="info-box">
                                        <div class="icon">
                                            <i data-feather="cast"></i>
                                        </div>
                                        <h4>Advertise A Job</h4>
                                        <p>Uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident.</p>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="info-box">
                                        <div class="icon">
                                            <i data-feather="layout"></i>
                                        </div>
                                        <h4>Recruiter Profiles</h4>
                                        <p>Uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident.</p>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="info-box">
                                        <div class="icon">
                                            <i data-feather="compass"></i>
                                        </div>
                                        <h4>Find Your dream job</h4>
                                        <p>Uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident.</p>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Candidates Details End -->
<?php endwhile; ?>

<?php get_footer();
