<?php


class ProductsService
{



    public static function getByCategory($offset = null, $posts_per_page = -1, $category_name = null, $post_type = 'post', $order = 'DESC'){
        $args = array(
            'post_type'=> $post_type,
            'category_name' => $category_name,
            'orderby'    => 'date',
            'post_status' => 'publish',
            'orderBy' => 'id',
            'order'    => $order,
            'offset' => $offset,
            'posts_per_page' => $posts_per_page
        );
        $result = new WP_Query( $args );
        return $result;
    }


    public static function getNews($offset = null, $posts_per_page = -1){
        return self::getByCategory($offset,$posts_per_page, 'Aktualnosci', 'post');
    }

    public static function getPagesByTemplate($value){
        return get_pages(array(
            'meta_key' => '_wp_page_template',
            'meta_value' => 'page-templates/'.$value
        ));
    }

    public static function getJobs()
    {
        return self::getByCategory(null,4,null,'job_listing', 'DESC');
    }

    public static function getNearJobs()
    {
        return self::getByCategory(null,4,null,'job_listing', "ASC");
    }

    public static function getJobsCategories(){
        return self::getByCategory(null,-1,null, 'jobs_categories')->posts;
    }

    public static function getLocations()
    {
        $result = ProductsService::getJobs();
        foreach($result->posts as $r){
            $locations[] = get_the_job_location($r);
        }
        return $locations;
    }


    public static function getCompanies(){
        return self::getByCategory(null,-1,null,'companies', 'DESC');

    }


}