<?php


class JobsController extends WP_REST_Controller
{
    /**
     * Rejestrujesz nowy routing metodą kopiuj/wklej
     * callback - funkcja która ma się wykonać
     */
    public function register_routes() {
        $namespace = 'jobs/v1';
        $base = 'route';
        register_rest_route( $namespace, '/jobsMainSearch/(?P<slug>[^.]+)', array(
            array(
                'methods'             => 'POST',
                'callback'            => array($this,'mainSearchJobs'),
            ),
        ) );
        register_rest_route( $namespace, '/jobsSearchByLoc/(?P<slug>[^.]+)', array(
            'methods' => 'POST',
            'callback' => array($this,'searchJobsByLocation'),
        ) );
        register_rest_route( $namespace, '/jobsSearchNear/', array(
            'methods' => 'POST',
            'callback' => array($this,'searchJobsNear'),
        ) );
        register_rest_route( $namespace, '/jobsSearchByLoc/', array(
            'methods' => 'POST',
            'callback' => array($this,'searchJobsDefault'),
        ) );
        register_rest_route( $namespace, '/jobsSearchByCat/(?P<slug>[^.]+)', array(
            'methods' => 'POST',
            'callback' => array($this,'searchJobsByCategory'),
        ) );
        register_rest_route( $namespace, '/jobsSearchBySide/(?P<slug>[^.]+)', array(
            'methods' => 'POST',
            'callback' => array($this,'searchJobsBySidebar'),
        ) );
        register_rest_route( $namespace, '/jobsSearch/', array(
            'methods' => 'POST',
            'callback' => array($this,'searchJobsDefault'),
        ) );
        register_rest_route( $namespace, '/jobsSearch/(?P<slug>[^.]+)', array(
            'methods' => 'POST',
            'callback' => array($this,'searchJobs'),
        ) );
        register_rest_route( $namespace, '/toPage/(?P<slug>[^.]+)', array(
            'methods' => 'POST',
            'callback' => array($this,'toPage'),
        ) );
    }

    public function searchJobsNear( $data ){
//        var_dump(array_key_first($data->get_params()));
        $args = array(
            'numberposts'	=> -1,
            'post_type'		=> 'job_listing',
            'meta_query'	=> array(
                'relation'		=> 'OR',
                array(
                    'key'		=> 'map',
                    'value'		=> substr(array_key_first($data->get_params()), 0, 5),
                    'compare'	=> 'LIKE'
                ),
            )
        );
        $result = new WP_Query( $args );

        return render('page-templates/parts/resultNear.php' ,$result->posts);
    }

    public function mainSearchJobs( $data ) {
        $args = array(
            'post_type' => 'job_listing',
            'orderby'    => 'ID',
            's' => $data['slug'],
            'post_status' => 'publish',
            'order'    => 'DESC',
            'posts_per_page' => 3
        );

        $posts = new WP_Query( $args );

        $args2 = array(
            'post_type' => 'jobs_categories',
            'orderby'    => 'ID',
            's' => $data['slug'],
            'post_status' => 'publish',
            'order'    => 'DESC',
            'posts_per_page' => 3
        );

        $cats = new WP_Query( $args2 );

        $content['posts'] = $posts->posts;
        $content['cats'] = $cats->posts;

        return render('page-templates/parts/resultMainSearch.php' ,$content);
    }

    public function searchJobs( $data ) {
        $args = array(
            'post_type' => 'job_listing',
            'orderby'    => 'ID',
            's' => $data['slug'],
            'post_status' => 'publish',
            'order'    => 'DESC',
            'posts_per_page' => -1
        );

        $result = new WP_Query( $args );

        return render('page-templates/parts/resultSearch.php' ,$result->posts);
    }

    public function toPage( $data ) {
        $args = array(
            'post_type' => 'page',
            'orderby'    => 'ID',
            's' => $data['slug'],
            'post_status' => 'publish',
            'order'    => 'DESC',
            'posts_per_page' => -1
        );

        $result = new WP_Query( $args );

        return render('page-templates/jobs-list.php' ,$result->posts);
    }

    public function searchJobsDefault( $data ) {
        $args = array(
            'post_type' => 'job_listing',
            'orderby'    => 'ID',
            'post_status' => 'publish',
            'order'    => 'DESC',
            'posts_per_page' => -1
        );

        $result = new WP_Query( $args );

        return render('page-templates/parts/resultSearch.php' ,$result->posts);
    }

    public function searchJobsByLocation( $data ){
        $args = array(
            'numberposts'	=> -1,
            'post_type'		=> 'job_listing',
            'meta_query'	=> array(
                'relation'		=> 'OR',
                array(
                    'key'		=> 'map',
                    'value'		=> $data['slug'],
                    'compare'	=> 'LIKE'
                ),
            )
        );
        $result = new WP_Query( $args );

        return render('page-templates/parts/resultSearch.php' ,$result->posts);
    }




    public function searchJobsByCategory( $data ){

        $args = array(
            'post_type' => 'jobs_categories',
            'orderby'    => 'ID',
            's' => $data['slug'],
            'post_status' => 'publish',
            'order'    => 'DESC',
            'posts_per_page' => 1
        );
        $categories = new WP_Query( $args );
        $category = $categories->posts[0];

        wp_reset_query();

        $args = array(
            'numberposts'	=> -1,
            'post_type'		=> 'job_listing',
            'meta_query'	=> array(
                'relation'		=> 'OR',
                array(
                    'key'		=> 'kategoria',
                    'value'		=> $category->ID,
                    'compare'	=> 'LIKE'
                ),
            )
        );
        $result = new WP_Query( $args );

        return render('page-templates/parts/resultSearch.php' ,$result->posts);
    }

    public function searchJobsBySidebar( $data ){
        $args = array(
            'numberposts'	=> -1,
            'post_type'		=> 'job_listing',
            'meta_query'	=> array(
                'relation'		=> 'OR',
                array(
                    'key'		=> 'doswiadczenie',
                    'value'		=> $data['slug'],
                    'compare'	=> 'LIKE'
                ),
            )
        );
        $result = new WP_Query( $args );

        return render('page-templates/parts/resultSearch.php' ,$result->posts);
    }

    /**
     * Get the query params for collections
     *
     * @return array
     */
    public function get_collection_params() {
        return array(
            'page'     => array(
                'description'       => 'Current page of the collection.',
                'type'              => 'integer',
                'default'           => 1,
                'sanitize_callback' => 'absint',
            ),
            'per_page' => array(
                'description'       => 'Maximum number of items to be returned in result set.',
                'type'              => 'integer',
                'default'           => 10,
                'sanitize_callback' => 'absint',
            ),
            'search'   => array(
                'description'       => 'Limit results to those matching a string.',
                'type'              => 'string',
                'sanitize_callback' => 'sanitize_text_field',
            ),
        );
    }
}