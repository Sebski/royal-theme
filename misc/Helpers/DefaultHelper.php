<?php


class DefaultHelper
{
    /**
     * Pobieranie info o kategorii np. ilość postów.
     * Nazwa kategorii
     * @param $category_name
     * @return string
     */
    static function getCountFromCategory($category_name){
        $category_id = get_cat_ID($category_name);
        $category = get_category($category_id);
        if($category->category_count > 1 && $category->category_count < 5){
            return $category->category_count . " dostępne preparaty";
        }elseif($category->category_count == 1){
            return $category->category_count . " dostępny preparat";
        }else{
            return $category->category_count . " dostępnych preparatów";
        }

    }

    static function tempDir() {
        return get_template_directory_uri();
    }

    static function imageDir( $string ) {
        return self::tempDir() . '/images/' . $string;
    }

    static function fileDir() {
        return get_template_directory();
    }

    static function DayDifference($start, $end) {
        $start = strtotime($start);
        $end = strtotime($end);
        $difference = $end - $start;
        return round($difference / 86400);
    }

    static function getCount($category){
        $args = array(
            'numberposts'	=> -1,
            'post_type'		=> 'job_listing',
            'meta_query'	=> array(
                'relation'		=> 'OR',
                array(
                    'key'		=> 'kategoria',
                    'value'		=> $category->ID,
                    'compare'	=> 'LIKE'
                ),
            )
        );
        $result = new WP_Query( $args );
        return $result->post_count;
    }

    static function isHome(){
        if(!is_front_page()){
            return 'other';
        } else {
            return '';
        }
    }

    static function isActive(){
        return true;
    }

}