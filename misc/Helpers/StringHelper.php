<?php



class StringHelper

{


    /**
     * Tak dla bezpieczeństwa...
     *
     * @param string $var
     * @return string
     */
    public function filterStr($var) {
        return trim(strip_tags(filter_var(
            str_replace(['<?php', '<?=', '?>', '<script>', '</script>'], '', $var)
        )));
    }

    public static function get_excerpt($limit, $post){

        $excerpt = the_excerpt($post);
        $excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
        $excerpt = strip_shortcodes($excerpt);
        $excerpt = strip_tags($excerpt);
        $excerpt = substr($excerpt, 0, $limit);
        $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
        $excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));
        return $excerpt;
    }

    public static function phone_to_link( $string ) {
        $string = str_replace( '+', '00', $string );
        $string = str_replace( '-', '', $string );
        $string = str_replace( ' ', '', $string );
        $string = str_replace( '.', '', $string );
        return $string;
    }

}