<?php


class MenuHelper
{
    function __construct(){
        add_action( 'init', 'registerMenu' );
    }

    public  function registerMenu() {
        register_nav_menus(
            array(
                'new-menu' => __( 'New Menu' ),
                'another-menu' => __( 'Another Menu' ),
            )
        );
    }
}