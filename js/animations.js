// $(document).ready(function() {
//     /*-----------------------------------
//       Move hero image depending on mouse position
//     //   -----------------------------------*/
//       $(document).ready(function () {
//         $('.banner-1').mousemove(function (e) {
//             // parallax(e, this, 1);
//             parallax(e, document.querySelector('.elementy'), 2);
//         });
//     });
//
//     function parallax(e, target, layer) {
//         var layer_coeff = 100 / layer;
//         var layer_coeff_H = 200 / layer;
//         var x = ($(window).width() - target.offsetWidth) / 2 - (e.pageX - ($(window).width() / 2)) / layer_coeff;
//         var y = ($(window).height() - target.offsetHeight) / 2 - (e.pageY - ($(window).height() / 2)) / layer_coeff_H;
//         $(target).offset({ top: y ,left : x });
//     };
// });

if (window.devicePixelRatio < 1.5 && window.innerWidth > 1000) {

    window.sr = ScrollReveal();

    sr.reveal('.banner-content', {
    distance: '150%',
    origin: 'left',
    opacity: null,
    delay: 1500,
    duration: 900
});

    sr.reveal('.searchAndFilter-wrapper', {
    distance: '150%',
    origin: 'left',
    opacity: null,
    delay: 100,
    duration: 900
});

    sr.reveal('.right', {
    distance: '150%',
    origin: 'right',
    opacity: null,
    delay: 200,
    duration: 900
});

    sr.reveal('.left', {
    distance: '150%',
    origin: 'left',
    opacity: null,
    delay: 200,
    duration: 900
});

    sr.reveal('.section-padding-top', {
    distance: '150%',
    origin: 'bottom',
    opacity: null,
    delay: 200,
    duration: 900
});

//     sr.reveal('.filtered-job-listing-wrapper', {
//     distance: '150%',
//     origin: 'right',
//     opacity: null,
//     delay: 200,
//     duration: 900
// });
//
//
//     sr.reveal('.job-filter-wrapper', {
//     distance: '150%',
//     origin: 'left',
//     opacity: null,
//     delay: 200,
//     duration: 900
// });


    sr.reveal('.col-xl-7', {
    distance: '150%',
    origin: 'left',
    opacity: null,
    delay: 200,
    duration: 900
});

    sr.reveal('.col-xl-4', {
    distance: '150%',
    origin: 'bottom',
    opacity: null,
    delay: 200,
    duration: 900
});
}
