const menu = document.querySelector('.navbar');
const image = document.querySelector('.img-responsive');
const docImage = document.querySelector('.doctor-image');
const pharImage = document.querySelector('.pharmacist-image');
const linkText = document.querySelectorAll('.link-text')
const text = document.querySelectorAll('.menu-item');
const language = document.querySelectorAll('.language');
const border = document.querySelector('.current-menu-item');


function hamburgerMenu () {
  const mobileMenu = document.querySelector('.mobile-wrapper');
  const mobileList = document.querySelector('.mobile-list');

  mobileMenu.addEventListener('click', () => {
    mobileList.classList.toggle('active-p');
  })
};

function load_main() {
  window.onload = () => {
    image.src = menu.querySelector('.img-responsive').src;
    // docImage.src = window.location.origin + "/wp-content/themes/vitamed/assets/img/ikona-dla-lekarzy-biala@2.png";
    // pharImage.src = window.location.origin +  "/wp-content/themes/vitamed/assets/img/ikona-dla-farmaceutow-biala@2.png";
    menu.classList.remove('secondary-header');
    menu.classList.remove('shadow');
    border.children[0].classList.remove('outline-alt');
    for (let i = 0; i < language.length; i++) {
      language[i].classList.remove('language-alt');
    }
    for (let i = 0; i < linkText.length; i++) {
      linkText[i].classList.remove('link-text-alt');
    }
    for (let i = 0; i < text.length; i++) {
      text[i].classList.remove('secondary-text');
      }
  }
}

load_main();

document.addEventListener("scroll", function onScrollHandler() {
  
  let scrollTop = document.documentElement.scrollTop || document.body.scrollTop;

  if (window.location.pathname == '/') {
    menu.classList.add('shadow');
  }
  
  //Działa, ale można poprawić
  if (scrollTop > 300) {

    image.src = window.location.origin + "/wp-content/themes/vitamed/assets/img/logo@2.png";
    docImage.src = window.location.origin + "/wp-content/themes/vitamed/assets/img/ikona-dla-lekarzy-granatowa@2.png";
    pharImage.src = window.location.origin + "/wp-content/themes/vitamed/assets/img/ikona-dla-farmaceutow-granatowa@2.png";
    menu.classList.add('secondary-header');
    menu.classList.add('shadow');
    border.children[0].classList.add('outline-alt');
    for (let i = 0; i < language.length; i++) {
      language[i].classList.add('language-alt');
    }
    for (let i = 0; i < linkText.length; i++) {
      linkText[i].classList.add('link-text-alt');
    }
    for (let i = 0; i < text.length; i++) {
    text[i].classList.add('secondary-text');
    }
  } else {
    image.src = menu.querySelector('.img-responsive').src;
    menu.classList.add('shadow');
    if (window.location.pathname == '/') {
      docImage.src = window.location.origin + "/wp-content/themes/vitamed/assets/img/ikona-dla-lekarzy-biala@2.png";
      pharImage.src = window.location.origin +  "/wp-content/themes/vitamed/assets/img/ikona-dla-farmaceutow-biala@2.png";
      border.children[0].classList.remove('outline-alt');
    }

    menu.classList.remove('secondary-header');
    menu.classList.remove('shadow');

    for (let i = 0; i < language.length; i++) {
      language[i].classList.remove('language-alt');
    }
    for (let i = 0; i < linkText.length; i++) {
      linkText[i].classList.remove('link-text-alt');
    }
    for (let i = 0; i < text.length; i++) {
      text[i].classList.remove('secondary-text');
      }
  }
  });





hamburgerMenu();

$(document).ready(function() {

    $('.search-icon').click(function () {
      if($(".search").css("display") == "none") {
        if ($(window).width() < 960) {
          $(".search-icon").hide();
          $(".img-responsive").attr("src",window.location.origin + "/wp-content/themes/vitamed/assets/img/logo-biale@2.png");
          $(".search").css({'margin-top':'33px'});
          $(".search").css({'margin-right':'80%'});

        }
      $('.search').animate(
          {
            width: '0px'
          }, 100, function () {
            $(this).hide();
            $('.search').animate(
                {
                  width: '250px'
                }, 1000, function () {
                  $(this).show();
                }
            );
          }
      );
      }else{

      }
    });

  });

