<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Royal - Created by Weblider</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/bootstrap.min.css">

    <!-- External Css -->
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/themify-icons.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/et-line.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/bootstrap-select.min.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/plyr.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/flag.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/slick.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/owl.carousel.min.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/jquery.nstSlider.min.css" />

    <!-- Custom Css -->
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/main.css">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600%7CRoboto:300i,400,500" rel="stylesheet">

    <!-- Favicon -->
    <link rel="icon" href="<?php bloginfo('template_directory'); ?>/images/favicon.png">
    <link rel="apple-touch-icon" href="<?php bloginfo('template_directory'); ?>/images/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo('template_directory'); ?>/images/icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo('template_directory'); ?>/images/icon-114x114.png">

    <!--[if lt IE 9]>
    <script src="<?php bloginfo('template_directory'); ?>/assets/js/html5shiv.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/assets/js/respond.min.js"></script>
    <![endif]-->

</head>
<body>

<!-- Header -->
<header class="header-2">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="header-top">
                    <div class="logo-area">
                        <a href="/"><img src="<?php bloginfo('template_directory'); ?>/images/logo.jpg" alt=""></a>
                    </div>
                    <div class="header-top-toggler">
                        <div class="header-top-toggler-button"></div>
                    </div>
                    <div class="top-nav">
                        <select class="selectpicker select-language" data-width="fit">
                            <option data-content='<span class="flag-icon flag-icon-us"></span> English'>English</option>
                            <option  data-content='<span class="flag-icon flag-icon-mx"></span> Español'>Español</option>
                        </select>
                    </div>
                </div>
                <nav class="navbar navbar-expand-lg cp-nav-2">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="pracownik-nav navbar-nav">
                            <li class="menu-item switch-button"><a style="color: #fff!important;" title="Home" href="#" class="btn btn-danger">Dla pracodawców</a></li>
                            <li class="menu-item "><a title="Home" href="/">Strona główna</a></li>
                            <li class="menu-item active"><a title="Home" href="/jobs-list">Oferty pracy</a></li>
                            <li class="menu-item "><a title="Home" href="/aplikuj-online">Aplikuj online</a></li>
                            <li class="menu-item "><a title="Home" href="/work-ukraine">Pracownicy z Ukrainy</a></li>
                            <li class="menu-item "><a title="Home" href="/work-abroad">Praca za granicą</a></li>
                            <li class="menu-item "><a title="Home" href="/about-us">O nas</a></li>
                            <li class="menu-item "><a title="Home" href="/faq">Regulamin</a></li>
                            <li class="menu-item "><a title="Home" href="/kontakt">Kontakt</a></li>
                        </ul>
                        <ul class="pracodawca-nav navbar-nav" style="display: none; white-space: nowrap;">
                            <li class="menu-item switch-button"><a style="color: #fff!important;" title="Home" href="#" class="btn btn-danger">Dla pracowników</a></li>
                            <li class="menu-item active"><a style="color: #6f7484 !important;" title="Home" href="/">Strona główna</a></li>
                            <li class="menu-item "><a style="color: #6f7484 !important;" title="Home" href="/rekrutacja">Rekrutacja</a></li>
                            <li class="menu-item "><a style="color: #6f7484 !important;" title="Home" href="/praca-tymczasowa">Praca tymczasowa</a></li>
                            <li class="menu-item "><a style="color: #6f7484 !important;" title="Home" href="/praca-stala">Praca stała</a></li>
                            <li class="menu-item "><a style="color: #6f7484 !important;" title="Home" href="/outsourcing">Outsourcing</a></li>
                            <li class="menu-item "><a style="color: #6f7484 !important;" title="Home" href="/consulting">Consulting</a></li>
                            <li class="menu-item "><a style="color: #6f7484 !important;" title="Home" href="/about-us">O nas</a></li>
                            <li class="menu-item "><a style="color: #6f7484 !important;" title="Home" href="/zapytaj-o-oferte">Zapytaj o ofertę</a></li>
                            <li class="menu-item "><a style="color: #6f7484 !important;" title="Home" href="/znajdz-nas">Znajdź nas</a></li>
                            <li class="menu-item "><a style="color: #6f7484 !important;" title="Home" href="/ogolna-oferta">Ogólna oferta</a></li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</header>