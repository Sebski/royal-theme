<?php
//renderowanko
require 'misc/Routing/JobsController.php';

$jobs = new JobsController();

 function render($template, Array $data) {

    extract($data);
    ob_start();

    include( $template);
    $content = ob_get_contents();
    ob_end_clean();

    return $content;
}

add_action( 'rest_api_init', function () use ($jobs) {
    $jobs->register_routes();
} );

function my_acf_google_map_api( $api ){

    $api['key'] = 'AIzaSyB832_bHfRQLmwHuIxiTy8K-5_REW1AAlY';

    return $api;

}

add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');

require 'misc/Helpers/DefaultHelper.php';


add_action('init', 'my_book_cpt');
function my_book_cpt()
{
    $args = array(
        'public'       => true,
        'show_in_rest' => true,
        'label'        => 'job_listing'
    );
    register_post_type('job_listing', $args);
}

function get_attach($obj, $name, $request)
{
    return get_attached_media('image', $obj['id']);
}

add_action('rest_api_init', function () {
    register_rest_field('job_listing', 'image', array('get_callback' => 'get_attach', 'update_callback' => null, 'schema' => null));
});

add_theme_support('job-manager-templates');

//Zarządzanie produktami
require 'misc/Services/ProductsService.php';


add_filter('nav_menu_css_class', 'special_nav_class', 10, 2);
add_action('init', 'the_post_thumbnail');
add_theme_support('post-thumbnails');
function special_nav_class($classes, $item)
{
    if (in_array('current-menu-item', $classes)) {
        $classes[] = 'active ';
    }
    return $classes;
}

function count_cat_post($category)
{
    if (is_string($category)) {
        $catID = get_cat_ID($category);
    } elseif (is_numeric($category)) {
        $catID = $category;
    } else {
        return 0;
    }
    $cat = get_category($catID);
    return $cat->count;
}


//limit przycięcia funkcji excerpt
function custom_excerpt_length($length)
{
    return 15;
}
add_filter('excerpt_length', 'custom_excerpt_length', 999);

//link czytaj wiecej funcji excerpt
function new_excerpt_more($more)
{
    return ' <a href="' . get_permalink(get_the_ID()) . '">Czytaj dalej >></a>';
}
