<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Royal - Created by Weblider</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/bootstrap.min.css">

    <!-- External Css -->
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/fonts/fonts.css">
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/themify-icons.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/et-line.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/bootstrap-select.min.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/plyr.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/flag.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/slick.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/owl.carousel.min.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/jquery.nstSlider.min.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/style.css" />

    <!-- Custom Css -->
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/main.css">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600%7CRoboto:300i,400,500" rel="stylesheet">

    <!-- Favicon -->
    <link rel="icon" href="<?php bloginfo('template_directory'); ?>/images/favicon.png">
    <link rel="apple-touch-icon" href="<?php bloginfo('template_directory'); ?>/images/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo('template_directory'); ?>/images/icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo('template_directory'); ?>/images/icon-114x114.png">


    <!--[if lt IE 9]>
    <script src="<?php bloginfo('template_directory'); ?>/assets/js/html5shiv.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/assets/js/respond.min.js"></script>
    <![endif]-->
    <style>
        html {
            overflow-x: hidden;
        }

        @media(max-width:720px) {
            body {
                overflow-x: hidden;
            }
        }
    </style>
</head>

<body onload="initAutocomplete(); initAutocomplete2(); getLocation();">

    <!-- Header -->
    <header>


        <nav class="navbar navbar-expand-xl absolute-nav transparent-nav cp-nav navbar-light bg-light fluid-nav">
            <a class="navbar-brand" href="/">
                <img src="<?php bloginfo('template_directory'); ?>/images/logo_przyklad.png" class="img-fluid header-logo" alt="">
            </a>
            <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse navbar-direction" id="navbarSupportedContent">
                <div class="language-change">
                    <!-- <?php var_dump(explode('/', $_SERVER["REQUEST_URI"])) ?> -->
                </div>
                <ul class="navbar-nav ml-auto main-nav switcher-container">
                    <li class="menu-item menu-switcher"><a class="switch-link-work" style="font-size: 12px; color: #000;" href="#">Dla pracowników</a></li>
                    <li class="menu-item menu-switcher"><a class="switch-link-jobs" style="font-size: 12px; color: #000;" href="#">Dla pracodawców</a></li>
                    <li>
                        <select class="selectpicker" onchange="location = this.value;" data-width="fit">
                            <?php if (get_locale() == 'pl_PL') { ?>
                                <option value="<?php echo  '/' . explode('/', $_SERVER["REQUEST_URI"])[2]; ?> " data-content='<a href="/"><span class="flag-icon flag-icon-pl"></span> Polski</a>'>Polski</option>
                                <option value="<?php echo site_url() . '/en/' . explode('/', $_SERVER["REQUEST_URI"])[1];  ?>" data-content='<a href="/en"><span class="flag-icon flag-icon-us"></span> English</a>'>English</option>
                                <option value="<?php echo site_url() . '/ru/' . explode('/', $_SERVER["REQUEST_URI"])[1];  ?>" data-content='<a href="/ru"><span class="flag-icon flag-icon-ru"></span> Russian</a>'>Russian</option>
                            <?php } else { ?>
                                <option value="<?php echo  '/' . explode('/', $_SERVER["REQUEST_URI"])[2]; ?> " data-content='<a href="/"><span class="flag-icon flag-icon-pl"></span> Polski</a>'>Polski</option>
                                <option value="<?php echo site_url() . '/en/' . explode('/', $_SERVER["REQUEST_URI"])[2];  ?>" data-content='<a href="/en"><span class="flag-icon flag-icon-us"></span> English</a>'>English</option>
                                <option value="<?php echo site_url() . '/ru/' . explode('/', $_SERVER["REQUEST_URI"])[2];  ?>" data-content='<a href="/ru"><span class="flag-icon flag-icon-ru"></span> Russian</a>'>Russian</option>
                            <?php } ?>
                        </select>
                    </li>
                </ul>
                <ul class="pracownik-nav navbar-nav ml-auto main-nav">
                    <li class="menu-item "><a class="menu-item-href" href="/">Strona główna</a></li>
                    <li class="menu-item "><a class="menu-item-href" href="/jobs-list/">Oferty pracy</a></li>
                    <li class="menu-item "><a class="menu-item-href" href="/o-nas/">O nas</a></li>
                    <li class="menu-item "><a class="menu-item-href" href="/faq/">Faq</a></li>
                    <li class="menu-item "><a class="menu-item-href" href="/kontakt/">Kontakt</a></li>
                </ul>
                <ul class="pracodawca-nav navbar-nav ml-auto main-nav" style="display: none">
                    <li class="menu-item "><a class="menu-item-href" href="/">Strona główna</a></li>
                    <li class="menu-item "><a class="menu-item-href" href="/o-nas/">O nas</a></li>
                    <!--                <li class="menu-item "><a class="menu-item-href" href="/oferta-ogolna/">Oferta ogolna</a></li>-->
                    <li class="menu-item "><a class="menu-item-href" href="/rekrutacja/">Rekrutacja</a></li>
                    <li class="menu-item "><a class="menu-item-href" href="/pracownik-tymczasowy/">Pracownik tymczasowy</a></li>
                    <li class="menu-item "><a class="menu-item-href" href="/outsourcing/">Outsourcing</a></li>
                    <li class="menu-item "><a class="menu-item-href" href="/consulting/">Consulting</a></li>
                    <li class="menu-item "><a class="menu-item-href" href="/faq/">Faq</a></li>
                    <li class="menu-item "><a class="menu-item-href" href="/kontakt/">Kontakt</a></li>
                </ul>
            </div>
        </nav>
    </header>




    <?php while (have_rows('box_quality')) : the_row();
        $desc = get_sub_field('desc');
        $img = get_sub_field('img');
    ?>
        <div class="single">
            <div class="desc">
                <p><?php echo $desc ?></p>
            </div>
        </div>
    <?php endwhile; ?>




    <?php while (have_rows('box_quality')) : the_row();
        $desc = get_sub_field('desc');
        $img = get_sub_field('img');
    ?>
        <div class="single">
            <div class="img">
                <img class="hex-img" src="<?php echo $img['url'] ?>" alt="<?php echo $img['url'] ?>">
            </div>
        </div>
    <?php endwhile; ?>