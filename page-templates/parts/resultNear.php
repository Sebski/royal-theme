<h2 class="blur-par text-center">Oferty w okolicy<span class="blur w-100" style="left:-10%">Oferty w okolicy.</span><span style="font-size:48px; color: #dc3545;">.</span></h2>
<p class="text-center" style="font-size:14px;" id="voivode"></p>
<?php foreach($data as $d) { ?>
    <div class="job-list half-grid">
        <div class="thumb">
            <a href="#">
                <img style="max-width:70px !important;" src="<?php echo get_the_post_thumbnail_url($d); ?>" class="img-fluid" alt="">
            </a>
        </div>
        <div class="body">
            <div class="content">
                <h4>
                    <a href="<?php echo get_the_permalink($d); ?>"><?php echo get_the_title($d); ?></a>
                </h4>
                <div class="info">
                    <span class="company"><a href="#"><i style="    color: rgba(220, 53, 69, 0.3);" class="<?php echo get_field('ikona',get_field('kategoria', $d)[0]->ID); ?>"> </i> <?php echo get_field('kategoria', $d)[0]->post_title; ?> </a> </span>
                    <span class="office-location"><a href="#"><i  style="color: rgba(0, 204, 0, 0.3);" class="fas fa-map-marker-alt"></i> <?php echo get_field('map', $d)['city']; ?>, <?php echo get_field('map', $d)['state']; ?></a></span>
                    <span class="job-type temporary"><a href="#"><i style="color: rgba(77, 121, 255, 0.5);" class="far fa-clock"> </i> <?php  echo wpjm_get_the_job_types($d)[0]->name; ?> </a></span>
                </div>
            </div>

        </div>
    </div>
<?php } ?>
<?php if (!$data) { ?>
    <p style="    text-align: center;
    padding-top: 35px;
    color: #dc3545;
    font-size: 20px;">Brak wyników dla danego województwa</p>

<?php } ?>
