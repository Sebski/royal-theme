<h5 class="pt-2 pb-2">Oferty pracy</h5>
<?php foreach ($data['posts'] as $d) { ?>
  <div class="job-list " style="background: white; margin: 0; padding:5px; z-index:999;">
    <div class="thumb" style="width:50px; height: 50px;">
      <a href="<?php echo get_the_permalink($d->ID); ?>">
        <img src="<?php echo get_the_post_thumbnail_url($d->ID); ?>" class="img-fluid" alt="">
      </a>
    </div>
    <div class="body">
      <div class="content">
        <h4 style="padding-top: 10px;"><a href="/job/<?php echo get_post($d->ID)->post_name; ?>"><?php echo get_the_title($d->ID); ?></a></h4>
        <div class="info">
          <span class="company"><a href="#"><i style="    color: rgba(220, 53, 69, 0.3);" class="<?php echo get_field('ikona',get_field('kategoria', $d)[0]->ID); ?>"> </i> <?php echo get_field('kategoria', $d)[0]->post_title; ?></a></span>
          <span class="office-location"><a href="#"><i  style="color: rgba(0, 204, 0, 0.3);" class="fas fa-map-marker-alt"></i> <?php echo get_field('map',$d)['city']; ?>, <?php echo get_field('map',$d)['state']; ?></a></span>
          <span class="job-type temporary"><a href="#"><i style="color: rgba(77, 121, 255, 0.5);" class="far fa-clock"> </i> <?php echo wpjm_get_the_job_types($d)[0]->name; ?></a></span>
        </div>
      </div>
      <div class="more">
        <p class="deadline">Wygasa: 24 Maja 2021 </p>
      </div>
    </div>
  </div>
<?php } ?>
<?php if (!$data['posts']) { ?>
  <p>Brak wyników</p>
<?php } ?>

<h5 class="pt-2 pb-2">Branże</h5>
<?php foreach ($data['cats'] as $d) { ?>
    <div class="job-list " style="background: white; margin: 0; padding:5px; z-index:999;text-align: center;
    margin: 0 auto;
    align-items: center;
    align-content: center;
    justify-content: center;">
        <i style="color: rgba(220, 53, 69, 0.3);font-size: 24px;" class="<?php echo get_field('ikona',$d); ?>"></i>
        <div class="body">
            <div class="content">
                <h4 style="padding-top: 10px;margin:5px;"><a href="/jobs-list/?cats=<?php echo get_the_title($d->ID); ?>"><?php echo get_the_title($d->ID); ?></a></h4>
            </div>
        </div>
    </div>
<?php } ?>
<?php if (!$data['cats']) { ?>
    <p>Brak wyników</p>
<?php } ?>
