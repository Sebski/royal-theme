<?php
/*
 * Template Name: Jobs List
 */

$keyword = $_GET['keyword'];

$location = $_GET['location'];

$cat = $_GET['cats'];

$isFounded = true;

get_header();
// args


if(!empty($location)){
    $args = array(
        'numberposts'	=> -1,
        'post_type'		=> 'job_listing',
        'meta_query'	=> array(
            'relation'		=> 'OR',
            array(
                'key'		=> 'map',
                'value'		=> serialize($location),
                'compare'	=> 'LIKE'
            ),
        )
    );
}else{
    $args = array(
        'post_type' => 'job_listing',
        'orderby'    => 'ID',
        's' => $keyword,
        'post_status' => 'publish',
        'order'    => 'DESC',
        'posts_per_page' => -1
    );
}


if($cat){
    $args = array(
        'post_type' => 'jobs_categories',
        'orderby'    => 'ID',
        's' => $cat,
        'post_status' => 'publish',
        'order'    => 'DESC',
        'posts_per_page' => 1
    );
    $categories = new WP_Query( $args );
    $category = $categories->posts[0];

    wp_reset_query();

    $args = array(
        'numberposts'	=> -1,
        'post_type'		=> 'job_listing',
        'meta_query'	=> array(
            'relation'		=> 'OR',
            array(
                'key'		=> 'kategoria',
                'value'		=> $category->ID,
                'compare'	=> 'LIKE'
            ),
        )
    );
}

// query
$result = new WP_Query( $args );
if(!$result->posts){
    $isFounded = false;
}
$jobsCategories = ProductsService::getJobsCategories();
$locations = ProductsService::getLocations();
?>

<div class="img-fluid position-absolute" style="z-index: 100; width: 127px; height: 167px; top: -2px; right: -13px; background-image: url(<?php echo DefaultHelper::imageDir('kafel.png'); ?>);">

</div>
<div class="img-fluid position-absolute" style="width: 212px; height: 166px; top: -26px; left: -63px; background-image: url(<?php echo DefaultHelper::imageDir('kafel2.png'); ?>);">

</div>

<!-- Breadcrumb -->
<div class="alice-bg padding-top-150 padding-bottom-70">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="breadcrumb-area">
                    <h1 style="font-family: 'Cy', sans-serif; color:#FEB254; font-size:5.6rem !important;"><?php the_title() ?></h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Strona główna</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><?php the_title() ?></li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="col-md-6">
                <div class="breadcrumb-form">
                    <form action="#">
                                <input
                                id="search-job"
                                data-method="POST"
                                data-fill="#job-filter-result"
                                data-url="/wp-json/jobs/v1/jobsSearch"
                                type="text"
                                placeholder="Wyszukaj...">
                        <button><i data-feather="search"></i></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Breadcrumb End -->



<!-- Job Listing -->
<div class="alice-bg section-padding-bottom">
    <div class="container">
        <div class="row no-gutters">
            <div class="col">
                <div class="job-listing-container">
                    <div class="filtered-job-listing-wrapper">
                        <div class="job-view-controller-wrapper">

                            <div class="showing-number">
                                <?php if(!$isFounded){ ?>
                                    <h5 class="text-center font-weight-light ">Brak ofert pracy dla tych kryteriów. Przejrzyj oferty pracy w twojej okolicy</h5>
                                    <?php
                                    $args = array(
                                        'post_type' => 'job_listing',
                                        'orderby'    => 'ID',
                                        's' => $keyword,
                                        'post_status' => 'publish',
                                        'order'    => 'DESC',
                                        'posts_per_page' => -1
                                    );
                                    $result = new WP_Query( $args );
                                    ?>
                                <?php }else{ ?>
                                    <span>Dostępne <?php echo $result->post_count; ?> oferty pracy</span>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="job-filter-result" id="job-filter-result">
                            <?php while ( $result->have_posts() ) : $result->the_post(); ?>
                                <div class="job-list">
                                    <div class="thumb">
                                        <a href="<?php the_permalink(); ?>">
                                            <img style="max-width:70px !important;" src="<?php echo get_the_post_thumbnail_url();?>" class="img-fluid" alt="">
                                        </a>
                                    </div>
                                    <div class="body">
                                        <div class="content">
                                            <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                                            <div class="info">
                                                <span class="company"><a href="#"><i style="    color: rgba(220, 53, 69, 0.3);" class="<?php echo get_field('ikona',get_field('kategoria', $post)[0]->ID); ?>"> </i>  <?php echo get_field('kategoria')[0]->post_title; ?></a></span>
                                                <span class="office-location"><a href="#"><i  style="color: rgba(0, 204, 0, 0.3);" class="fas fa-map-marker-alt"> </i> <?php echo get_field('map')['city']; ?>, <?php echo get_field('map')['state']; ?></a></span>
                                                <span class="job-type temporary"><a href="#"><i style="color: rgba(77, 121, 255, 0.3);" class="far fa-clock"> </i> <?php echo wpjm_get_the_job_types($post)[0]->name; ?></a></span>
                                            </div>
                                        </div>
                                        <div class="more">
                                            <div class="buttons">
                                                <a href="<?php the_permalink(); ?>" class="button">Przejrzyj ofertę</a>
<!--                                                <a href="#" class="favourite"  data-toggle="modal" data-target="#apply-popup-id"><i data-feather="heart"></i></a>-->
                                            </div>
                                            <p class="deadline">Wygasa: 10 Luty,  2020</p>
                                        </div>
                                    </div>
                                </div>
                            <?php endwhile; ?>
                            <div class="apply-popup">
                                <div class="modal fade" id="apply-popup-id" tabindex="-1" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title"><i data-feather="edit"></i>Aplikuj na ofertę</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form action="#">
                                                    <div class="form-group">
                                                        <input type="text" placeholder="Imię i nazwisko" class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="email" placeholder="Adres email" class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <textarea class="form-control" placeholder="Wiadomość"></textarea>
                                                    </div>
                                                    <div class="form-group file-input-wrap">
                                                        <label for="up-cv">
                                                            <input id="up-cv" type="file">
                                                            <i data-feather="upload-cloud"></i>
                                                            <span>Dodaj CV <span>(pdf,zip,doc,docx)</span></span>
                                                        </label>
                                                    </div>
                                                    <div class="more-option">
                                                        <input class="custom-radio" type="checkbox" id="radio-4" name="termsandcondition">
                                                        <label for="radio-4">
                                                            <span class="dot"></span> Akceptuję <a href="#">regulamin</a>
                                                        </label>
                                                    </div>
                                                    <button class="button primary-bg btn-block">Aplikuj teraz </button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="pagination-list text-center">
                            <nav class="navigation pagination">
                                <div class="nav-links">
                                    <a class="prev page-numbers" href="#"><i class="fas fa-angle-left"></i></a>
                                    <a class="page-numbers" href="#"><span aria-current="page" class="page-numbers current">1</a>
                                    <a class="page-numbers" href="#">2</span></a>
                                    <a class="page-numbers" href="#">3</a>
                                    <a class="page-numbers" href="#">4</a>
                                    <a class="next page-numbers" href="#"><i class="fas fa-angle-right"></i></a>
                                </div>
                            </nav>
                        </div>
                    </div>
                    <div class="job-filter-wrapper">
                        <div class="selected-options same-pad">
                            <div class="selection-title">
                                <h4>Wybrane:</h4>
                                <a href="#" id="clear">Wyczyść</a>
                            </div>
                            <ul class="filtered-options">
                            </ul>
                        </div>
                        <div class="job-filter-dropdown same-pad category">
                            <select id="selectCategory" class="selectpicker"
                                    data-method="POST"
                                    data-fill="#job-filter-result"
                                    data-url="/wp-json/jobs/v1/jobsSearchByCat">
                                <option value="" selected>Kategorie</option>
                                <?php foreach($jobsCategories as $jobCategory) { ?>
                                <option value="<?php echo get_the_title($jobCategory); ?>"><?php echo get_the_title($jobCategory); ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="job-filter-dropdown same-pad location">
                            <input type="text" class="w-100" name="location-side" id="selectLocation" class="selectpicker"
                                   data-method="POST"
                                   data-fill="#job-filter-result"
                                   data-url="/wp-json/jobs/v1/jobsSearchByLoc" placeholder="Lokalizacja"
                            style="width: 100%;
    border: 0;
    border-radius: 0;
    border-bottom: 2px solid rgba(220, 53, 69, 0.2);
    background: transparent;
    outline: none;
    height: 50px;
    padding: 0;
    transition: all .3s ease;"
                            />
                        </div>
                        <div data-id="job-type" class="job-filter job-type same-pad">
                            <h4 class="option-title">Rodzaj zatrudnienia</h4>
                            <ul>
                                <li class="full-time"><i data-feather="clock"></i><a href="#" data-attr="Pełen etat">Pełen etat</a></li>
                                <li class="part-time"><i data-feather="clock"></i><a href="#" data-attr="Pół etatu">Pół etatu</a></li>
                                <li class="freelance"><i data-feather="clock"></i><a href="#" data-attr="Staż">Staż</a></li>
                            </ul>
                        </div>
                        <div data-id="experience" class="job-filter experience same-pad">
                            <h4 class="option-title">Doświadczenie</h4>
                            <ul>
                                <li><a href="#" data-attr="0">Brak</a></li>
                                <li><a href="#" data-attr="1">Mniej niż rok</a></li>
                                <li><a href="#" data-attr="2">2 lata</a></li>
                                <li><a href="#" data-attr="3">3 lata</a></li>
                                <li><a href="#" data-attr="4">4 lata</a></li>
                                <li><a href="#" data-attr="5">5 lat</a></li>
                                <li><a href="#" data-attr="6">Ponad 5 lat</a></li>
                            </ul>
                        </div>
<!--                        <div class="job-filter same-pad">-->
<!--                            <h4 class="option-title">Salary Range</h4>-->
<!--                            <div class="price-range-slider">-->
<!--                                <div class="nstSlider" data-range_min="0" data-range_max="10000"-->
<!--                                     data-cur_min="0"    data-cur_max="6130">-->
<!--                                    <div class="bar"></div>-->
<!--                                    <div class="leftGrip"></div>-->
<!--                                    <div class="rightGrip"></div>-->
<!--                                    <div class="grip-label">-->
<!--                                        <span class="leftLabel"></span>-->
<!--                                        <span class="rightLabel"></span>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
                        <div data-id="post" class="job-filter post same-pad">
                            <h4 class="option-title">Data dodania</h4>
                            <ul>
                                <li><a href="#" data-attr="Ostatnia godzina">Ostatnia godzina</a></li>
                                <li><a href="#" data-attr="Ostatnie 24 godziny">Ostatnie 24 godziny</a></li>
                                <li><a href="#" data-attr="Ostatnie 7 dni">Ostatni tydzień</a></li>
                                <li><a href="#" data-attr="Ostatnie 30 dni">Ostatni miesiąc</a></li>
                            </ul>
                        </div>
<!--                        <div data-id="qualification" class="job-filter qualification same-pad">-->
<!--                            <h4 class="option-title">Qualification</h4>-->
<!--                            <ul>-->
<!--                                <li><a href="#" data-attr="Matriculation">Matriculation</a></li>-->
<!--                                <li><a href="#" data-attr="Intermidiate">Intermidiate</a></li>-->
<!--                                <li><a href="#" data-attr="Gradute">Gradute</a></li>-->
<!--                            </ul>-->
<!--                        </div>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Job Listing End -->

<!-- NewsLetter -->
    <div class="newsletter-bg newsletter-mobile padding-top-90 mt-5 mb-5 ">
        <div class="container">
            <div class="row">
                <div class="col-xl-5 col-lg-6">
                    <div class="newsletter-wrap">
                        <h2>Masz pytania?</h2>
                        <h6>+48 123 123 123</h6>
                        <h3>Skontaktuj się z nami!</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- NewsLetter End -->

<?php get_footer(); ?>
